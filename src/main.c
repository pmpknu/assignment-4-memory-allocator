#include <assert.h>
#include <sys/mman.h>

#include "mem.h"
#include "mem_internals.h"

#define DEFAULT_HEAP_SIZE 4096
#define HEAP_START ((void*)0x04040000)

void debug(const char* fmt, ...);

static struct block_header *get_header(void *contents) {
  return (struct block_header *)(((uint8_t *)contents) -
                                 offsetof(struct block_header, contents));
}
void* map_pages(void const* addr, size_t length, int additional_flags) {
    return mmap((void*) addr, length, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | additional_flags, -1, 0);
}

void* initialize_heap(size_t size) {
    void* heap = heap_init(size);
    assert(heap);
    debug("Heap initialized: %d bytes\n", size);
    return heap;
}

void allocate_memory(size_t size) {
    debug("Memory allocation: %d bytes...\n", size);
    void* allocated = _malloc(size);
    assert(allocated);
    debug("Allocation success\n");
}

void terminate_heap() {
    heap_term();
}

// Test function for allocating a memory block
void test_malloc_block() {
    debug("TEST 1: malloc_block run...\n");

    initialize_heap(DEFAULT_HEAP_SIZE);

    allocate_memory(2048);
    allocate_memory(8192);

    terminate_heap();
    debug("TEST 1: passed!\n");
}

// Test function for freeing a single block
void test_free_block() {
    debug("TEST 2: free_block run...\n");

    initialize_heap(DEFAULT_HEAP_SIZE);

    void* first_block = _malloc(10);
    void* second_block = _malloc(20);
    void* third_block = _malloc(30);

    _free(second_block);

    assert(!get_header(first_block)->is_free);
    assert(get_header(second_block)->is_free);
    assert(!get_header(third_block)->is_free);

    terminate_heap();
    debug("TEST 2: passed!\n");
}

// Test function for freeing multiple blocks
void test_free_blocks() {
    debug("TEST 3: free_blocks run...\n");

    initialize_heap(DEFAULT_HEAP_SIZE);

    void* first_block = _malloc(10);
    void* second_block = _malloc(20);

    _free(first_block);
    _free(second_block);

    assert(get_header(first_block)->is_free);
    assert(get_header(second_block)->is_free);

    terminate_heap();
    debug("TEST 3: passed!\n");
}

// Test function for expanding a region
void test_expand_region() {
    debug("TEST 4: expand_region run...\n");

    struct region* heap = heap_init(DEFAULT_HEAP_SIZE);
    size_t initial_region_size = heap->size;

    _malloc(3 * 4096);
    size_t expanded_region_size = heap->size;

    assert(initial_region_size < expanded_region_size);

    terminate_heap();
    debug("TEST 4: passed!\n");
}

// Test function for expanding a filled region
void test_expand_filled_region() {
    debug("TEST 5: expand_filled_region run...\n");

    void* pre_allocated = map_pages(HEAP_START, 10, MAP_FIXED);
    assert(pre_allocated);

    void* allocated_filled_ptr = _malloc(10);
    assert(allocated_filled_ptr);
    assert(pre_allocated != allocated_filled_ptr);

    terminate_heap();
    debug("TEST 5: passed!\n");
}

int main() {
    test_malloc_block();
    test_free_block();
    test_free_blocks();
    test_expand_region();
    test_expand_filled_region();

    debug("ALL TESTS PASSED\n");
    return 0;
}
